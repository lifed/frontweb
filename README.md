## frontvue3
[Mazer](https://github.com/zuramai/mazer)  

[Demo Page](https://lifed.gitlab.io/frontweb)

### 安裝模組
```
npm install
```

### 執行Server
```
npm run serve
```

## json-server
安裝步驟請依網站指示 [json-server](https://www.npmjs.com/package/json-server)

### 啟動
```bash
json-server --watch db.json
```

