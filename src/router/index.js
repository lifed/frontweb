import {createRouter, createWebHistory} from 'vue-router'

// {
//     path: '/FeatherIcons',
//         name: 'FeatherIcons',
//     component: () => import('../views/FeatherIconsView'),
//     meta: {
//     requiresAuth: true,
// }
// },
// {
//     path: '/Table',
//         name: 'Table',
//     component: () => import('../views/TableView'),
//     meta: {
//     requiresAuth: true,
// }
// },
// {
//     path: '/Alert',
//         name: 'Alert',
//     component: () => import('../views/AlertView'),
//     meta: {
//     requiresAuth: true,
// }
// },
// {
//     path: '/Badge',
//         name: 'Badge',
//     component: () => import('../views/ComponentBadge'),
//     meta: {
//     requiresAuth: true,
// }
// },
const routes = [
    // name & path must be unique
    {
        path: '/',
        // redirect will not render children what path is empty by name.
        // redirect: {
        //     name: 'Index'
        // },
        // it's works
        // redirect: "/modules",
        redirect: {
            name: 'Dashboard'
        },
    },
    {
        path: '/modules',
        name: 'Index',
        component: () => import('@/views/Modules/ModuleFrame'),
        meta: {
            requiresAuth: true,
        },
        children: [
            {
                path: '',
                name: 'Dashboard',
                component: () => import('@/views/Modules/Dashboard'),
            }
        ]
    },
    {
        path: '/Badge',
        name: 'Badge',
        component: () => import('@/views/Modules/badgemodule/ComponentBadge'),
        meta: {
            requiresAuth: true,
        }
    },
    {
        path: '/:pathMatch(.*)*',
        name: 'NotFound',
        component: () => import('@/views/Modules/httpstatuscode/404')
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import('@/views/LoginView')
    },
    {
        path: '/register',
        name: 'Register',
        component: () => import('@/views/RegisterView')
    },
    {
        path: '/forgetpassword',
        name: 'ForgetPassword',
        component: () => import('@/views/ForgetPassword')
    },
    {
        path: '/logout',
        name: 'Logout',
        component: () => import('@/views/LogoutController')
    },
]

const router = createRouter({
    // https://router.vuejs.org/zh/guide/essentials/history-mode.html
    history: createWebHistory(process.env.BASE_URL),
    routes,
    // 預設router-link-{exact-}active指定成特定關鍵字，如；active
    // linkActiveClass: 'active',
    // linkExactActiveClass: 'active'
})

const isLoggedIn = (JSON.parse(localStorage.getItem("isLoggedIn")) === true)
// console.log('localStorage - > ' + typeof(localStorage.getItem("isLoggedIn"))); string
// console.log('localStorage=== jsonparse - > ' + (JSON.parse(localStorage.getItem("isLoggedIn")) === true)); true

// https://router.vuejs.org/guide/advanced/meta.html
router.beforeEach((to) => {
    if (to.meta.requiresAuth && !isLoggedIn) {
        return {
            name: 'Login',
        }
    } else {
        return true;
    }
})
export default router
