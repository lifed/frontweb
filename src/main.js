import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import Vuex from 'vuex'

import 'bootstrap/dist/js/bootstrap.bundle'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-icons/font/bootstrap-icons.css'
import 'perfect-scrollbar'
import 'perfect-scrollbar/css/perfect-scrollbar.css'

// mazer custom css in order
import '@/assets/css/mazer/bootstrap.css'
import '@/assets/css/mazer/app.css'

// import VueApexCharts from 'vue3-apexcharts'
const app = createApp(App)
app.use(router)
    .use(Vuex)
    // .use(VueApexCharts)
    .mount('#app')
